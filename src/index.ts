require('dotenv').config();
import mongoose from 'mongoose';
import { app } from './app';
import { DatabaseConnectionError } from '@goodfood/common';
import { natsWrapper } from './nats-wrapper';
import { ProductCreatedListener } from './events/listeners/product-created-listeners';
import { ProductUpdatedListener } from './events/listeners/product-updated-listeners';
import { PaymentCreatedListener } from './events/listeners/payment-created-listener';
import { DeliveryCreatedListener } from './events/listeners/delivery-created-listener';
import { DeliveryCompletedListener } from './events/listeners/delivery-completed-listener';
import { DeliveryDeletedListener } from './events/listeners/delivery-deleted-listener';

const PORT = 3000;

const start = async () => {
  if (process.env.NODE_ENV === 'test') return;

  if (!process.env.MONGO_URI) {
    throw new Error('MONGO_URI must be defined');
  }

  if (!process.env.JWT_KEY) {
    throw new Error('JWT_KEY must be provided !');
  }

  if (!process.env.NATS_CLIENT_ID) {
    throw new Error('NATS_CLIENT_ID must be provided !');
  }

  if (!process.env.NATS_URL) {
    throw new Error('NATS_URL must be provided !');
  }

  if (!process.env.NATS_CLUSTER_ID) {
    throw new Error('NATS_CLUSTER_ID must be provided !');
  }

  try {
    await natsWrapper.connect(
      process.env.NATS_CLUSTER_ID,
      process.env.NATS_CLIENT_ID,
      process.env.NATS_URL
    );

    natsWrapper.client.on('close', () => {
      console.log('NATS connection closed !');
      process.exit();
    });

    process.on('SIGINT', () => natsWrapper.client.close());
    process.on('SIGTERM', () => natsWrapper.client.close());

    new ProductCreatedListener(natsWrapper.client).listen();
    new ProductUpdatedListener(natsWrapper.client).listen();
    new PaymentCreatedListener(natsWrapper.client).listen();
    new DeliveryCreatedListener(natsWrapper.client).listen();
    new DeliveryCompletedListener(natsWrapper.client).listen();
    new DeliveryDeletedListener(natsWrapper.client).listen();

    //TODO: test listeners (19 - 9)

    await mongoose.connect(process.env.MONGO_URI);

    console.log('Connected to mongoDb');
  } catch (err) {
    throw new DatabaseConnectionError();
  }

  app.listen(PORT, () => {
    console.log(`Listening on port ${PORT} !`);
  });
};

start();
