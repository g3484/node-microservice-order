import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import { errorHandler, NotFoundError, currentUser } from '@goodfood/common';

import { CreateOrderRouter } from './routes/new';
import { DeleteOrderRouter } from './routes/delete';
import { ListOrdersRouter } from './routes';
import { OrderByIdRouter } from './routes/show';
import { OrdersCancelledRouter } from './routes/stats/countOrdersCancelled';
import { OrdersTopOfProductsRouter } from './routes/stats/topOfProductSold';
import { TurnoverRouter } from './routes/stats/turnover';

const app = express();
app.use(json());

app.use(currentUser);

app.all('/api/orders/health', (req, res) => {
  res.send('K8S is healthy');
});

app.use(CreateOrderRouter);
app.use(DeleteOrderRouter);
app.use(ListOrdersRouter);
app.use(OrdersCancelledRouter);
app.use(OrdersTopOfProductsRouter);
app.use(TurnoverRouter);
app.use(OrderByIdRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };
