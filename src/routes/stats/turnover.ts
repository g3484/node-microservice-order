import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { Order } from '../../models/order';
import {
  requireAuth,
  OrderStatus,
  BadRequestError,
  validateRequest,
} from '@goodfood/common';

const router = express.Router();

router.get(
  '/api/orders/stats/turnover',
  requireAuth,
  async (req: Request, res: Response) => {
    const { date, period } = req.query;

    if (typeof date !== 'string' || typeof period !== 'string') {
      throw new BadRequestError('Invalid query params');
    }

    let orders;

    if (period === 'day') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(new Date(date).setDate(new Date(date).getDate() - 1)),
          $lte: new Date(new Date(date).setDate(new Date(date).getDate() + 1)),
        },
      });
    }

    if (period === 'week') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(new Date(date).setDate(new Date(date).getDate() - 7)),
          $lte: new Date(new Date(date).setDate(new Date(date).getDate() + 1)),
        },
      });
    }

    if (period === 'month') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(
            new Date(date).setMonth(new Date(date).getMonth() - 1)
          ),
          $lte: new Date(new Date(date).setDate(new Date(date).getDate() + 1)),
        },
      });
    }

    if (period === 'year') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(
            new Date(date).setFullYear(new Date(date).getFullYear() - 1)
          ),
          $lte: new Date(new Date(date).setDate(new Date(date).getDate() + 1)),
        },
      });
    }

    if (!orders) {
      throw new BadRequestError('No orders found');
    }

    const price = orders.reduce((acc, order) => acc + order.price, 0);

    return res.status(200).send({ turnover: price });
  }
);

export { router as TurnoverRouter };
