import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { Order } from '../../models/order';
import { requireAuth, OrderStatus, BadRequestError } from '@goodfood/common';

const router = express.Router();

router.get(
  '/api/orders/stats/topProducts',
  requireAuth,
  async (req: Request, res: Response) => {
    const { date, period } = req.query;

    if (typeof date !== 'string' || typeof period !== 'string') {
      throw new BadRequestError('Invalid query params');
    }

    let orders;

    if (period === 'day') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(new Date(date).setDate(new Date(date).getDate() - 1)),
          $lte: new Date(date),
        },
      });
    }

    if (period === 'week') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(date),
          $lt: new Date(date).setDate(new Date(date).getDate() + 7),
        },
      });
    }

    if (period === 'month') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(
            new Date(date).setMonth(new Date(date).getMonth() - 1)
          ),
          $lte: new Date(date),
        },
      });
    }

    if (period === 'year') {
      orders = await Order.find({
        status: OrderStatus.Completed,
        dateOfOrder: {
          $gte: new Date(
            new Date(date).setFullYear(new Date(date).getFullYear() - 1)
          ),
          $lte: new Date(date),
        },
      });
    }

    if (!orders) {
      throw new BadRequestError('No orders found');
    }

    const products = orders.map((order) => order.products);

    const productsFlat = products.flat();

    const productsCount = productsFlat.reduce(
      (acc: { [key: string]: { name: string; count: number } }, product) => {
        if (acc[product._id]) {
          acc[product._id].count++;
        } else {
          acc[product._id] = { name: product.name, count: 1 };
        }
        return acc;
      },
      {}
    );

    const productsCountArray = Object.values(productsCount);

    const productsCountSorted = productsCountArray.sort(
      (a, b) => b.count - a.count
    );

    const topFiveProducts = productsCountSorted.slice(0, 5);

    return res.status(200).send(topFiveProducts);
  }
);

export { router as OrdersTopOfProductsRouter };
