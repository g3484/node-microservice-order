import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import {
  requireAuth,
  validateRequest,
  ServerError,
  NotFoundError,
} from '@goodfood/common';
import { Product } from '../models/product';
import { Order } from '../models/order';
import { OrderStatus } from '../types/order-enum';
import { OrderCreatedPublisher } from '../events/publishers/order-created-publisher';
import { natsWrapper } from '../nats-wrapper';

const router = express.Router();

router.post(
  '/api/orders',
  requireAuth,
  [
    body('userName')
      .not()
      .isEmpty()
      .isString()
      .withMessage('userName is required'),
    body('address')
      .not()
      .isEmpty()
      .isString()
      .withMessage('Address is required'),
    body('productIds')
      .not()
      .isEmpty()
      .isArray()
      .withMessage('productIds must be provided'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { productIds, address, userName } = req.body;

    const products = [];

    for (const productId of productIds) {
      const product = await Product.findById(productId);
      if (!product) {
        throw new NotFoundError();
      }

      products.push(product);
    }

    try {
      const price = products.reduce((acc, product) => {
        return acc + product.price;
      }, 0);

      const order = Order.build({
        userId: req.currentUser!.id,
        userName: userName,
        address: address,
        products: products,
        price,
        status: OrderStatus.Created,
        dateOfOrder: new Date(),
      });

      await order.save();

      new OrderCreatedPublisher(natsWrapper.client).publish({
        id: order.id,
        userId: order.userId,
        userName: order.userName,
        address: order.address,
        price: order.price,
        status: order.status,
        dateOfOrder: order.dateOfOrder,
        version: order.version,
      });

      res.status(201).send(order);
    } catch (err) {
      throw new ServerError();
    }
  }
);

export { router as CreateOrderRouter };
