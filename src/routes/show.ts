import express from 'express';
import { NotAuthorizedError, NotFoundError } from '@goodfood/common';
import { Order } from '../models/order';
import { requireAuth } from '@goodfood/common';

const router = express.Router();

//Recherche d'un produit par son id
router.get('/api/orders/:id', requireAuth, async (req, res) => {
  const order = await Order.findById(req.params.id);

  if (!order) {
    throw new NotFoundError();
  }

  if (order.userId !== req.currentUser!.id) {
    throw new NotAuthorizedError();
  }

  res.status(200).send(order);
});

export { router as OrderByIdRouter };
