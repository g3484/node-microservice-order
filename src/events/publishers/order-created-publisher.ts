import { Publisher, OrderCreatedEvent, Subjects } from '@goodfood/common';

export class OrderCreatedPublisher extends Publisher<OrderCreatedEvent> {
  subject: Subjects.OrderCreated = Subjects.OrderCreated;
}
