import { Message } from 'node-nats-streaming';
import { Subjects, Listener, ProductCreatedEvent } from '@goodfood/common';
import { Product } from '../../models/product';
import { queueGroupName } from './queue-group-name';

export class ProductCreatedListener extends Listener<ProductCreatedEvent> {
  subject: Subjects.ProductCreated = Subjects.ProductCreated;
  queueGroupName = queueGroupName;

  async onMessage(data: ProductCreatedEvent['data'], msg: Message) {
    const { id, name, price, imageUrl } = data;

    const product = Product.build({
      id,
      name,
      imageUrl,
      price,
    });

    await product.save();

    msg.ack();
  }
}
