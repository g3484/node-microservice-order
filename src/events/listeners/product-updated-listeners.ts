import { Message } from 'node-nats-streaming';
import { Subjects, Listener, ProductUpdatedEvent } from '@goodfood/common';
import { Product } from '../../models/product';
import { queueGroupName } from './queue-group-name';

export class ProductUpdatedListener extends Listener<ProductUpdatedEvent> {
  subject: Subjects.ProductUpdated = Subjects.ProductUpdated;
  queueGroupName = queueGroupName;

  async onMessage(data: ProductUpdatedEvent['data'], msg: Message) {
    const { id, version, name, price } = data;

    const product = await Product.findByEvent({ id, version });

    if (!product) {
      throw new Error('Product not found');
    }

    product.set({
      name,
      price,
    });

    await product.save();

    msg.ack();
  }
}
