# GoodFood - NodeJS => Orders

## Description

GoodFood est une solution de livraison de plats cuisinés.

Cette solution comporte un site web ainsi qu'une application mobile, et est composée de plusieurs micro services effectuant des actions spécifiques.

Dans ce repository, le microservice NodeJS gère les catégories de produit et les produits de la solution GoodFood.

### Fonctionnalités

Le microservice NodeJS inclus les fonctionnalités suivantes :

- **Lister les commandes** : /api/orders
- **Voir le détail d'une commande selon son identifiant** : /api/orders/:id
- **Création d'une commande** : \*/api/orders => Les paramètres qu'il y a besoin sont : userName, Address, ProductsId
- **Suppression d'une commande (Commande Annulée) selon son identifiant** : \*/api/orders/:id

* => Routes privées accessible avec le JWT
